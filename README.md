
 
# Social Posts

## Getting started:

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

## Live version:
[CLICK](https://glacial-castle-33131.herokuapp.com)

## Screenshoots:
![social1.jpg](https://bitbucket.org/repo/XX96MBK/images/3912908447-social1.jpg)
![social2.jpg](https://bitbucket.org/repo/XX96MBK/images/724799980-social2.jpg)