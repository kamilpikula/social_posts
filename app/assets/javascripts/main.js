$(document).on('turbolinks:load', function() {
    $('.dropdown-trigger').dropdown(); //dropdown menu
    $('textarea#textarea1').characterCounter(); //counter for textarea micropost
    $('.sidenav').sidenav(); //sidenav for mobile devices
    $('.materialboxed').materialbox();

    'use strict';

    var cardsHowWorks = document.querySelectorAll('.card-how-works');
    function hoverCard() {
        if (this.classList.contains('blue')) {
            this.classList.remove('blue', 'darken-4', 'white-text');
            this.classList.add('transparent', 'blue-text', 'text-darken-4');
        } else {
            this.classList.remove('transparent', 'blue-text', 'text-darken-4');
            this.classList.add('blue', 'darken-4', 'white-text');
        }
    }
    cardsHowWorks.forEach(function (card) {
        return card.addEventListener('mouseenter', hoverCard);
    });
    cardsHowWorks.forEach(function (card) {
        return card.addEventListener('mouseleave', hoverCard);
    });
});
