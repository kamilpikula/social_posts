require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:kamil)
  end

  test "profile display" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'shared/_user_info'
    assert_select 'title', full_title(@user.name)
    assert_select 'span', text: @user.name
    assert_select 'img.circle' # img with gravatar
    assert_match @user.microposts.count.to_s, response.body
    assert_select 'ul.pagination'
  end
end
