require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SampleApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.eager_load_paths += %W(#{config.root}/lib/validators)
    config.eager_load_paths << Rails.root.join('lib')
    config.assets.initialize_on_precompile = false
    config.action_view.field_error_proc = Proc.new { |html_tag, instance|
      html_tag
    }
    # include autheticity token in remote forms
    config.action_view.embed_authenticity_token_in_remote_forms = true
  end
end
