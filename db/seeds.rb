# Users
User.create!(name: "Kamil Pikula",
             email: "pikusix1@o2.pl",
             password: "abc123",
             password_confirmation: "abc123",
             activated: true,
             activated_at: Time.zone.now)

User.create!(name: "Admin Admin",
             email: "admin@social.com",
             password: "adminadmin",
             password_confirmation: "adminadmin",
             activated: true,
             activated_at: Time.zone.now,
             admin: true)

# Random users
32.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@rails.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

# Microposts
users = User.order(:created_at).take(2)
35.times do
  content = Faker::Lorem.sentence(7)
  users.each { |user| user.microposts.create!(content: content) }
end

# Relationship
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }